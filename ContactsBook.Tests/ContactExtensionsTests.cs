﻿using AddressBook.Extensions;
using AddressBook.Models;
using System;
using Xunit;

namespace AddressBook.Tests
{
    public class ContactExtensionsTests
    {
        private readonly Contact _contact;
        public ContactExtensionsTests()
        {
            _contact = new Contact("Slava", "Tester", "tester@akvelon.com", "");
        }

        [Theory]
        [InlineData(true, "Slava")]
        [InlineData(false, "False")]
        [InlineData(false, null)]
        [InlineData(false, "")]
        [InlineData(true, "Tester")]
        [InlineData(true, "akvelon.com")]
        public void TestContainsExtensionMultipleInlineData(bool expectedResult, string keyword)
        {
            Assert.Equal(expectedResult, _contact.Contains(keyword));
        }

        [Fact]
        public void TestChangeFirstNameExtensionSuccsess()
        {
            var originalFName = _contact.FirstName;
            var newFName = "Petr";
            var updatedContact = _contact.ChangeFirstName(newFName);
            Assert.Equal(originalFName, _contact.FirstName);
            Assert.Equal(newFName, updatedContact.FirstName);
            Assert.Equal(_contact.LastName, updatedContact.LastName);
            Assert.Equal(_contact.Email, updatedContact.Email);
            Assert.Equal(_contact.PhoneNumber, updatedContact.PhoneNumber);
        }
        [Fact]
        public void TestChangeFirstNameExtensionThrowsNullArgumentException()
        {
            Assert.Throws<ArgumentNullException>(() => _contact.ChangeFirstName(null));
        }


        [Fact]
        public void TestChangeLastNameExtensionSuccsess()
        {
            var originalLName = _contact.LastName;
            var newLName = "Petrosyan";
            var updatedContact = _contact.ChangeLastName(newLName);
            Assert.Equal(originalLName, _contact.LastName);
            Assert.Equal(newLName, updatedContact.LastName);
            Assert.Equal(_contact.FirstName, updatedContact.FirstName);
            Assert.Equal(_contact.Email, updatedContact.Email);
            Assert.Equal(_contact.PhoneNumber, updatedContact.PhoneNumber);
        }

        [Fact]
        public void TestChangeLastNameExtensionThrowsNullArgumentException()
        {
            Assert.Throws<ArgumentNullException>(() => _contact.ChangeLastName(null));
        }

        [Fact]
        public void TestChangeEmailExtensionSuccsess()
        {
            var originalEmail = _contact.Email;
            var newEmail = "petr@gmail.com";
            var updatedContact = _contact.ChangeEmail(newEmail);
            Assert.Equal(originalEmail, _contact.Email);
            Assert.Equal(newEmail, updatedContact.Email);
            Assert.Equal(_contact.FirstName, updatedContact.FirstName);
            Assert.Equal(_contact.LastName, updatedContact.LastName);
            Assert.Equal(_contact.PhoneNumber, updatedContact.PhoneNumber);
        }

        [Fact]
        public void TestChangeEmailExtensionThrowsNullArgumentException()
        {
            Assert.Throws<ArgumentNullException>(() => _contact.ChangeEmail(null));
        }

        [Fact]
        public void TestChangePhoneNumberExtensionSuccsess()
        {
            var originalPhoneNumber = _contact.PhoneNumber;
            var newPhoneNumber = "96582523255";
            var updatedContact = _contact.ChangePhoneNumber(newPhoneNumber);
            Assert.Equal(originalPhoneNumber, _contact.PhoneNumber);
            Assert.Equal(newPhoneNumber, updatedContact.PhoneNumber);
            Assert.Equal(_contact.FirstName, updatedContact.FirstName);
            Assert.Equal(_contact.LastName, updatedContact.LastName);
            Assert.Equal(_contact.Email, updatedContact.Email);
        }

        [Fact]
        public void TestChangePhoneNumberExtensionThrowsNullArgumentException()
        {
            Assert.Throws<ArgumentNullException>(() => _contact.ChangePhoneNumber(null));
        }
    }
}
