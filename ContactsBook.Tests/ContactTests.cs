using AddressBook.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace AddressBook.Tests
{
    public class ContactTests
    {

        [Theory]
        [MemberData(nameof(TestDataForConstuctorParamsValidationTest))]
        public void TestConstructorParamsValidationTheory(string fName, string lName, string email, string phone)
        {
            var ex = Assert.Throws<ArgumentNullException>(() => new Contact(fName, lName, email, phone));
        }

        [Theory]
        [MemberData(nameof(Test1TestData))]
        public void TestContactGetHashCodeTheory(bool expected, Contact first, Contact second)
        {
            Assert.Equal(expected, first.GetHashCode() == second.GetHashCode());
        }

        [Theory]
        [MemberData(nameof(Test1TestData))]
        public void TestContactEqualsOnGetHashCodeDataTheory(bool expected, Contact first, Contact second)
        {
            Assert.Equal(expected, first.Equals(second));
        }

        [Theory]
        [MemberData(nameof(TestDataForEqualsWithAnotherTypesTest))]
        public void TestContactEqualsTheory(bool expected, Contact first, object second)
        {
            Assert.Equal(expected, first.Equals(second));
        }

        public static IEnumerable<object[]> TestDataForConstuctorParamsValidationTest()
        {
            yield return new object[] { null, "", "", ""};
            yield return new object[] { null, null, "", ""};
            yield return new object[] { null, null, null, ""};
            yield return new object[] { null, null, null, null};
        }

        public static IEnumerable<object[]> Test1TestData()
        {
            yield return new object[] { true, new Contact("", "", ""), new Contact("", "", "") };
            yield return new object[] { false, new Contact("", "", ""), new Contact("", "", "", "") };
            yield return new object[] {
                true,
                new Contact("Slava", "Shendryk", "slava@email.com"),
                new Contact("Slava", "Shendryk", "slava@email.com")
            };
            yield return new object[] {
                true,
                new Contact("Slava", "Shendryk", "slava@email.com"),
                new Contact("slava", "shendryk", "slava@email.com")
            };
            yield return new object[] {
                false,
                new Contact("Slava", "Shendryk", "slava@email.com"),
                new Contact("Shendryk", "Slava", "slava@email.com")
            };
            yield return new object[] {
                true,
                new Contact("Slava", "Shendryk", "slava@email.com", ""),
                new Contact("Slava", "Shendryk", "slava@email.com", "")
            };
        }

        public static IEnumerable<object[]> TestDataForEqualsWithAnotherTypesTest()
        {
            yield return new object[] { false, new Contact("", "", ""), new object() };
            yield return new object[] { false, new Contact("", "", ""), "Wrong" };
            yield return new object[] { false, new Contact("", "", ""), 5 };
            yield return new object[] { false, new Contact("", "", ""),  null };
        }
    }
}
