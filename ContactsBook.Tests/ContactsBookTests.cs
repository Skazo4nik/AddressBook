﻿using AddressBook.Extensions;
using AddressBook.Interfaces;
using AddressBook.Models;
using System;
using Xunit;

namespace AddressBook.Tests
{

    public class ContaxtsBookFixture : ContactsBook
    {
        public ContaxtsBookFixture()
        {
            AddContacts(new[] {
                new Contact("Slava", "Shendryk", "test@email.com"),
                new Contact("Admin", "", "admin@email.com"),
                new Contact("Vasya", "Test", "test1@email.com")
            });

            Assert.Equal(3, Count);
        }
    }

    public class ContactsBookTests : IClassFixture<ContaxtsBookFixture>
    {
        public readonly IContactsBook _contactsBook;

        public ContactsBookTests(ContaxtsBookFixture contactsBook)
        {
            _contactsBook = contactsBook;
        }

        [Fact]
        public void TestAddingContactsOneByOneWithDuplicates()
        {
            var currentCount = _contactsBook.Count;
            _contactsBook.AddContact(new Contact("Slava", "Shendryk", "test@email.com"));
            Assert.Equal(currentCount, _contactsBook.Count);
            _contactsBook.AddContact(new Contact("Apostol", "Petr", "petr@email.com"));
            Assert.Equal(currentCount + 1, _contactsBook.Count);
        }

        [Fact]
        public void TestGetFirstItemWithoutPredicate()
        {
            var first = _contactsBook.First();
            Assert.NotNull(first);
        }

        [Fact]
        public void TestGetFirstItemWithPredicate()
        {
            var name = "Slava";
            var firstWithPredicate = _contactsBook.First(contact => contact.FirstName == name);
            Assert.NotNull(firstWithPredicate);
            Assert.Equal(name, firstWithPredicate.FirstName);
        }

        [Fact]
        public void TestGetLastItem()
        {
            var last = _contactsBook.Last();
            Assert.NotNull(last);
        }

        [Fact]
        public void TestGetLastItemWithPredicate()
        {
            var name = "Admin";
            var lastWithPredicate = _contactsBook.Last(contact => contact.FirstName == name);
            Assert.NotNull(lastWithPredicate);
            Assert.Equal(name, lastWithPredicate.FirstName);
        }

        [Fact]
        public void TestContainsContact()
        {
            var contact = new Contact("test", "Contains", "te@ggg.com");
            _contactsBook.AddContact(contact);
            Assert.True(_contactsBook.Contains(contact));
        }
        [Fact]
        public void TestContainsWithNullThrowNullArgException()
        {
            Assert.Throws<ArgumentNullException>(() => _contactsBook.Contains(null));
        }

        [Fact]
        public void TestUpdateContact()
        {
            var contact = new Contact("Test", "Update", "update@gmail.com");
            _contactsBook.AddContact(contact);
            Assert.True(_contactsBook.Contains(contact));

            var updatedContact = contact.ChangeLastName("Updated");
            Assert.False(_contactsBook.Contains(updatedContact));

            _contactsBook.UpdateContact(contact, updatedContact);
            Assert.False(_contactsBook.Contains(contact));
            Assert.True(_contactsBook.Contains(updatedContact));
        }

        [Fact]
        public void TestUpdateContactWithNullArguments()
        {
            var contact = new Contact("Test", "Update", "update@gmail.com");
            Assert.Throws<ArgumentNullException>(() => _contactsBook.UpdateContact(null, contact));
            Assert.Throws<ArgumentNullException>(() => _contactsBook.UpdateContact(contact, null));
            Assert.Throws<ArgumentNullException>(() => _contactsBook.UpdateContact(null, null));
        }

        [Fact]
        public void TestDeleteMethod()
        {
            var contact = new Contact("Test", "Delete", "delete@g.com");
            _contactsBook.AddContact(contact);
            Assert.True(_contactsBook.Contains(contact));
            _contactsBook.DeleteContact(contact);
            Assert.False(_contactsBook.Contains(contact));
        }

        [Fact]
        public void TestDeleteMethodWithNullArgument()
        {
            Assert.Throws<ArgumentNullException>(() => _contactsBook.DeleteContact(null));
        }
    }
}
