﻿using AddressBook.Extensions;
using AddressBook.Interfaces;
using AddressBook.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AddressBook
{
    public class ContactsBook : IContactsBook
    {
        private ICollection<Contact> _store;
        public int Count => _store.Count;

        public ContactsBook()
        {
            _store = new HashSet<Contact>();
        }

        public virtual void AddContact(Contact contact)
        {
            if (contact == null)
                throw new ArgumentNullException(nameof(contact));

            _store.Add(contact);
        }

        public virtual void AddContacts(IEnumerable<Contact> contacts)
        {
            if (contacts == null)
                throw new ArgumentNullException(nameof(contacts));

            foreach (var contact in contacts)
                AddContact(contact);
        }

        public virtual void DeleteContact(Contact contact)
        {
            if (contact == null)
                throw new ArgumentNullException(nameof(contact));

            _store.Remove(contact);
        }

        public virtual IEnumerable<Contact> GetContracts()
        {
            return _store;
        }

        public virtual void OrderBy<TResult>(Func<Contact, TResult> selector, bool orderByDecreasing = false)
        {
            throw new NotImplementedException();
        }

        public virtual IEnumerable<Contact> Search(string keyWord)
        {
            if (string.IsNullOrEmpty(keyWord))
                throw new ArgumentException($"{keyWord} can not be null or empty");

            return _store.Where(contact => contact.Contains(keyWord));
        }

        public virtual void UpdateContact(Contact oldContact, Contact newContact)
        {
            if (oldContact == null)
                throw new ArgumentNullException(nameof(oldContact));
            if (newContact == null)
                throw new ArgumentNullException(nameof(newContact));

            if (oldContact.GetHashCode() == newContact.GetHashCode())
                return;

            DeleteContact(oldContact);
            AddContact(newContact);
        }

        public virtual bool Contains(Contact contact)
        {
            if (contact == null)
                throw new ArgumentNullException(nameof(contact));

            return _store.Contains(contact);
        }

        public IEnumerable<Contact> Where(Func<Contact, bool> predicate)
        {
            if (predicate == null)
                return GetContracts();

            return _store.Where(predicate);
        }

        public Contact First()
        {
            return _store.FirstOrDefault();
        }

        public Contact First(Func<Contact, bool> predicate)
        {
            if (predicate == null)
                return First();

            return _store.FirstOrDefault(predicate);
        }

        public Contact Last()
        {
            return _store.LastOrDefault();
        }

        public Contact Last(Func<Contact, bool> predicate)
        {
            if (predicate == null)
                return Last();

            return _store.LastOrDefault(predicate);
        }
    }
}
