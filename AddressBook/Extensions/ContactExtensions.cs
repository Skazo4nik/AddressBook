﻿using AddressBook.Exceptions;
using AddressBook.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AddressBook.Extensions
{
    public static class ContactExtensions
    {
        public static bool Contains(this Contact contact, string keyWord)
        {
            if (string.IsNullOrEmpty(keyWord))
                return false;

            return ((contact.FirstName != null) && contact.FirstName.Contains(keyWord, StringComparison.InvariantCultureIgnoreCase))
                || ((contact.LastName != null) && contact.LastName.Contains(keyWord, StringComparison.InvariantCultureIgnoreCase))
                || ((contact.Email != null) && contact.Email.Contains(keyWord, StringComparison.InvariantCultureIgnoreCase))
                || ((contact.PhoneNumber != null) && contact.PhoneNumber.Contains(keyWord, StringComparison.InvariantCultureIgnoreCase));
        }

        public static Contact ChangeFirstName(this Contact contact, string firstName)
        {
            return contact.PhoneNumber == null
                ? new Contact(firstName, contact.LastName, contact.Email)
                : new Contact(firstName, contact.LastName, contact.Email, contact.PhoneNumber);
        }

        public static Contact ChangeLastName(this Contact contact, string lastName)
        {
            return contact.PhoneNumber == null 
                ? new Contact(contact.FirstName, lastName, contact.Email)
                : new Contact(contact.FirstName, lastName, contact.Email, contact.PhoneNumber);
        }

        public static Contact ChangeEmail(this Contact contact, string email)
        {
            return contact.PhoneNumber == null
                ? new Contact(contact.FirstName, contact.LastName, email)
                : new Contact(contact.FirstName, contact.LastName, email, contact.PhoneNumber);
        }

        public static Contact ChangePhoneNumber(this Contact contact, string phoneNumber)
        {
            return new Contact(contact.FirstName, contact.LastName, contact.Email, phoneNumber);
        }
    }
}
