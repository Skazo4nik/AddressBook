﻿using AddressBook.Models;
using System;
using System.Collections.Generic;

namespace AddressBook.Interfaces
{
    public interface IContactsBook : IContactsBookCRUD, ISearchableContactsBook, IOrderableContactsBook
    {
        public int Count { get; }
    }

    public interface IContactsBookCRUD
    {
        void AddContact(Contact contact);
        void AddContacts(IEnumerable<Contact> contracts);
        void DeleteContact(Contact contact);
        IEnumerable<Contact> GetContracts();
        void UpdateContact(Contact oldContact, Contact newContact);
    }

    public interface ISearchableContactsBook
    {
        IEnumerable<Contact> Search(string keyWord);
        bool Contains(Contact contact);
        IEnumerable<Contact> Where(Func<Contact, bool> predicate);
        Contact First();
        Contact First(Func<Contact, bool> predicat);
        Contact Last();
        Contact Last(Func<Contact, bool> predicat);
    }

    public interface IOrderableContactsBook
    {
        void OrderBy<TResult>(Func<Contact, TResult> selector, bool orderByDecreasing = false);
    }
}
