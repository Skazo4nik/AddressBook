﻿using AddressBook.Models;
using System;

namespace AddressBook
{
    class Program
    {
        static void Main(string[] args)
        {
            var book = new ContactsBook();
            book.AddContact(new Contact("Slava", "Shendryk", "viacheslav.shendryk@akvelon.com"));
            book.AddContact(new Contact("Max", "Bogdan", "max.bogdan@akvelon.com"));
            Console.WriteLine(book.Count);
            Console.ReadKey();
        }

        private static void PrintContactsBook(ContactsBook book)
        {
            var contacts = book.GetContracts();
            foreach (var contact in contacts)
                Console.WriteLine(contact.ToString());
        }
    }
}
