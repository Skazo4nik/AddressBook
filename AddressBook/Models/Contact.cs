﻿using AddressBook.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace AddressBook.Models
{
    public class Contact
    {
        public Contact(string firstName, string lastName, string email)
        {
            FirstName = firstName ?? throw new ArgumentNullException(nameof(firstName));
            LastName = lastName ?? throw new ArgumentNullException(nameof(lastName));
            Email = email ?? throw new ArgumentNullException(nameof(email));
        }

        public Contact(string firstName, string lastName, string email, string phone)
            : this(firstName, lastName, email)
        {
            PhoneNumber = phone ?? throw new ArgumentNullException(nameof(phone));
        }

        public string FirstName { get; }
        public string LastName { get; }
        public string DisplayName { get => $"{FirstName} {LastName}"; }
        public string Email { get; }
        public string PhoneNumber { get; }

        public override int GetHashCode()
        {
            int hash = 13;
            hash = (hash * 7) + FirstName.ToLower().GetHashCode();
            hash = (hash * 7) + LastName.ToLower().GetHashCode();
            hash = (hash * 7) + Email.ToLower().GetHashCode();
            hash = (hash * 7) + (PhoneNumber == null ? 0 : PhoneNumber.ToLower().GetHashCode());

            return hash;
        }

        public override bool Equals(object obj)
        {
            return obj is Contact 
                ? (obj as Contact).GetHashCode() == GetHashCode() 
                : false;
        }

        public override string ToString()
        {
            return $"{DisplayName} | {Email} | {PhoneNumber}";
        }
    }
}
