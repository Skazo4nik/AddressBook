﻿using AddressBook.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AddressBook.Exceptions
{
    public class ContactAlreadyExistException : Exception
    {
        public ContactAlreadyExistException() 
            : base($"Contract Book already contains this contact")
        {
        }
    }

    public class ContactNotFound : Exception
    {
        public ContactNotFound() : base("Contact book does not contain this contact")
        {
        }
    }
}
