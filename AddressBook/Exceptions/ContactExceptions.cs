﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AddressBook.Exceptions
{
    public class ArgumentNullOrEmptyException : ArgumentException
    {
        public ArgumentNullOrEmptyException(string param)
            : base($"{param} can not be null or empty")
        {
        }
    }
}
